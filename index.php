<?php

/*** Demarrage de la session ***************************************************/

session_start();

/** Initialisation de l'autoloading et du router ******************************/

require('src/Autoloader.php');
Autoloader::register();

$router = new router\Router(basename(__DIR__));

/** Définition des routes *****************************************************/

// GET "/"

$router->get('/', 'controller\IndexController@index');

$router->get('/store', 'controller\StoreController@store');

$router->get('/store/{:num}', function($id){controller\StoreController::product($id);});

$router->get('/account', 'controller\AccountController@account');

$router->get('/account/logout','controller\AccountController@logout');

$router->get('/account/info','controller\AccountController@infos');

$router->get('/cart','controller\CartController@cart');

// POST "/"

$router->post('/account/login','controller\AccountController@login');

$router->post('/account/signin','controller\AccountController@signin');

$router->post('/postComment/{:num}', function($id){controller\CommentController::postComment($id);});

$router->post('/search','controller\StoreController@search');

$router->post('/account/update','controller\AccountController@update');

$router->post('/store/cart/add/{:num}',function($id){controller\CartController::add($id);});

$router->post('/cart/update','controller\CartController@update');


// Erreur 404
$router->whenNotFound('controller\ErrorController@error');

/** Ecoute des requêtes entrantes *********************************************/

$router->listen();
