<?php
if(isset($_GET['status'])){
if($_GET['status']=="update_success"){
    ?>
    <div class="box info" style="margin: 30px">
        Tes informations personnelles ont été mises à jour !
    </div>
    <?php
}
}
?>
<div id="infos" style="margin: 64px 32px">
    <h2 style="margin-bottom: 16px;">Informations du compte</h2>

    <h3 style="margin: 32px 0 16px;">Informations personnelles</h3>

    <form method="post" action="/account/update">
        <table>
            <tbody>
            <tr>
                <td>Prénom</td>
                <td><input type="text" id="firstname" name="firstname" value=<?=$_SESSION['firstname']?>></td>
            </tr>
            <tr>
                <td>Nom</td>
                <td><input type="text" id="lastname" name="lastname" value=<?=$_SESSION['lastname']?>></td>
            </tr>
            <tr>
                <td>Adresse mail</td>
                <td><input type="text" id="mail" name="mail" value="<?=$_SESSION['mail']?>"></td>
            </tr>
            <tr>
                <td>
                    <input type="submit" style="margin-bottom: 30px" value="Modifier mes informations">
                </td>
            </tr>
            </tbody>
        </table>




        <h3>Commandes</h3>

        <p>Tu n'as pas de commande en cours</p>
    </form>

</div>

<script src="../../../public/scripts/infos.js"></script>