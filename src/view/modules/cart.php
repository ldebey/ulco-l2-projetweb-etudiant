<?php $carts = $params['cart']?>
<?php if(isset($_SESSION['firstname'])){

?>
<div id="cart">
    <h2>Panier</h2>
    <form method="post" action="/cart/update">
        <table>
            <?php
            if($carts != null){
                $i=0;
                foreach ($carts as $cart){
                    ?>
                    <tr class="number[<?= $i?>]">
                        <td><img src="/public/images/<?= $cart["image"]?>"></td>
                        <td>
                            <p class="category"><?= $cart["category"]?></p>
                            <p class="product"><?= $cart["name"]?></p>
                        </td>
                        <td>
                            <p style="margin-bottom: 12px;">Quantité :</p>
                            <div>
                                <button type="button" class="group[<?= $i?>]" id="-">-</button>
                                <button type="button" class="group[<?= $i?>]" id="affiche_cpt" disabled><?= $_SESSION['cart']['cpt'][array_search($cart['id'],$_SESSION['cart']['id'])]?></button>
                                <input type="text" name="cpt[<?= $i?>]" id="input_cpt" class="cpt group[<?= $i?>]" style="visibility: hidden; display: none" value="<?= $_SESSION['cart']['cpt'][array_search($cart['id'],$_SESSION['cart']['id'])]?>">
                                <input type="text" name="id_cart[<?= $i?>]" style="display: none" value="<?= $cart['id']?>">
                                <button type="button" class="group[<?= $i?>]" id="+">+</button>
                            </div>
                        </td>
                        <td>
                            <p style="margin-bottom: 12px;">Prix unitaire :</p>
                            <p class="price">
                                <?= $cart["price"]?> €
                            </p>
                        </td>
                    </tr>
                        <?php
                    $i++;
                    }
                }
                else{
                    ?>
                    <h3 style="margin-top: 20px">Ton panier est vide</h3>
                    <?php
                }
                ?>

        </table>
        <div id="box-price">
            <p style="margin-bottom: 12px;">Prix total du panier:</p>
            <p id="total"></p>
        </div>
        <input type="submit" value="Procéder au paiement">
    </form>
</div>

<script src="../../../public/scripts/cart.js"></script>

<?php }
else{
    ?>
        <div id="cart">
            <h2><a href="/account">Connectez-vous</a> pour poster un avis.</h2>
        </div>
<?php
}
?>