<?php $product = $params['product']?>
<div id="product">
    <div>
        <div class="product-images">
            <img src="/public/images/<?= $product["image"]?>">
            <div class="product-miniatures">
                <div><img src="/public/images/<?= $product["image"]?>"></div>
                <div><img src="/public/images/<?= $product["image_alt1"]?>"></div>
                <div><img src="/public/images/<?= $product["image_alt2"]?>"></div>
                <div><img src="/public/images/<?= $product["image_alt3"]?>"></div>
            </div>
        </div>
        <div class="product-infos">
            <p class="product-category">
                <?= $product["category"]?>
            </p>
            <h1> <?= $product["name"]?> </h1>
            <p class="product-price">
                <?= $product["price"]?> €
            </p>
            <form id="form" method="post" action="cart/add/<?= $product['id']?>">
                <button type="button" id="-">-</button>
                <button type="button" id="affiche_cpt" disabled>1</button>
                <input type="text" name="cpt" id="input_cpt" style="visibility: hidden; display: none" value="">
                <button type="button" id="+">+</button>
                <input type="submit" value="Ajouter au panier">
            </form>
        </div>
    </div>
    <div>
        <div class="product-spec">
            <h2> Spécificités </h2>
            <?= $product["spec"]?>
        </div>
        <form method="post" action="/postComment/<?= $product['id']?>">
            <div class="product-comments">
                <h2>Avis</h2>
                <?php
                $tab = model\CommentModel::listComment($product['id']);
                if(sizeof($tab)!=0){
                    foreach ($tab as $i){
                        ?>
                            <div class="product-comment">
                                <p class="product-comment-author" style="margin-top: 10px"> <?= $i['firstname']?> <?= $i['lastname']?></p>
                                <p style="margin-top: 5px"><?= $i['content']?></p>
                            </div>

                        <?php
                    }
                }
                else{
                ?>
                <p style="margin-top: 10px">Il n'y a pas d'avis pour ce produit</p>
                <?php
                }
                if(isset($_SESSION['firstname'])){


                ?>
                <input type="text" name="comment" placeholder="Rédiger un commentaire" style="margin-top: 20px">
                <input type="submit" style="margin-top: 10px">
                <?php
                }
                else{


                ?>
                    <p><a href="/account">Connectez-vous</a> pour poster un avis.</p>
                <?php
                }
                ?>
            </div>
        </form>
    </div>
</div>

<script src="../../../public/scripts/product.js"></script>
