<nav>
    <img src="/public/images/logo.jpeg">
    <a href="/">Accueil</a>
    <a href="/store">Boutique</a>
    <a href="/account" class="account">
        <img src="/public/images/avatar.png">
    </a>
    <?php
    if(isset($_SESSION['firstname'])){
        ?>
        <a href="/account/info"><?= $_SESSION['firstname']?> <?= $_SESSION['lastname']?></a>
        <a href="/cart">Panier</a>
        <a href="/account/logout">Déconnexion</a>
    <?php
    }
    ?>
</nav>