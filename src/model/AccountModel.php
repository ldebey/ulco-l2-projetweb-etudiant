<?php


namespace model;

class AccountModel{
    static function check($firstname,$lastname,$mail,$password):bool{
        if(strlen($firstname)>=2){
            if(strlen($lastname)>=2){
                if(filter_var($mail, FILTER_VALIDATE_EMAIL)){
                    //Adresse mail unique dans la table SQL
                    if(strlen($password)>=6){
                        return true;
                    }
                }
            }
        }
        return false;
    }

    static function signin($firstname,$lastname,$mail,$password):bool{
        if(self::check($firstname,$lastname,$mail,$password)){
            // Connexion à la base de données
            $db = \model\Model::connect();

            // Requête SQL
            $sql = "INSERT INTO account (firstname,lastname,mail,password) VALUES (:firstname,:lastname,:mail,:password)";

            // Exécution de la requête
            $req = $db->prepare($sql);
            $req->execute(['firstname'=>$firstname,'lastname'=>$lastname,'mail'=>$mail,'password'=>$password]);
            return true;
        }
        return false;
    }

    static function login($mail,$password):bool{
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT id , firstname , lastname , mail , password FROM account WHERE mail = :mail";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(['mail'=>$mail]);

        $tab = $req->fetch();

        if($tab!=null && password_verify($password,$tab['password'])){
            $_SESSION['id'] = $tab['id'];
            $_SESSION['firstname'] = $tab['firstname'];
            $_SESSION['lastname'] = $tab['lastname'];
            $_SESSION['mail'] = $tab['mail'];
            return true;
        }
        return false;
    }

    static function update($firstname,$lastname,$mail){
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "UPDATE account SET account.firstname = :firstname , account.lastname = :lastname , account.mail = :mail WHERE account.id = :account";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(['firstname' => htmlspecialchars($firstname), 'lastname' => htmlspecialchars($lastname) ,'mail'=>htmlspecialchars($mail) ,'account' => $_SESSION['id']]);

        $_SESSION['firstname'] = $firstname;
        $_SESSION['lastname'] = $lastname;
        $_SESSION['mail'] = $mail;
    }
}