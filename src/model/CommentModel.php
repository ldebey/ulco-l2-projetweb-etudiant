<?php


namespace model;


class CommentModel
{
    static function insertComment($id_product,$id_account,$content){
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "INSERT INTO comment (id_product,id_account,content,date) VALUES (:id_product,:id_account,:content,:date)";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(['id_product' => $id_product , 'id_account' => $id_account , 'content' => $content , 'date' => date('Y-m-d H:i:s')]);
    }

    static function listComment($id_product): array
    {
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT account.firstname as firstname , account.lastname as lastname , comment.content as content FROM comment INNER JOIN account ON comment.id_account = account.id WHERE comment.id_product = :id_product ";

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute(['id_product'=>$id_product]);

        return $req->fetchAll();
    }
}