<?php

namespace model;

class StoreModel {

  static function listCategories(): array
  {
    // Connexion à la base de données
    $db = \model\Model::connect();

    // Requête SQL
    $sql = "SELECT id, name FROM category";
    
    // Exécution de la requête
    $req = $db->prepare($sql);
    $req->execute();

    // Retourner les résultats (type array)
    return $req->fetchAll();
  }

  static function listProducts(): array{

      // Connexion à la base de données
      $db = \model\Model::connect();

      // Requête SQL
      $sql = "SELECT product.id as id, product.name as name, product.price as price, product.image as image , category.name AS category  FROM product INNER JOIN category ON product.category = category.id";

      // Exécution de la requête
      $req = $db->prepare($sql);
      $req->execute();

      // Retourner les résultats (type array)
      return $req->fetchAll();
  }

    static function listSearch($name,$cat,$crois,$decrois):array{
        // Connexion à la base de données
        $db = \model\Model::connect();


        // Requête SQL
        $sql = "SELECT product.id as id, product.name as name, product.price as price, product.image as image , category.name AS category  FROM product INNER JOIN category ON product.category = category.id ";
        if($cat!=null){
            for($i=0;$i<sizeof($cat);$i++){
                if(sizeof($cat)>1){
                    if($i==0){
                        $sql .= "WHERE category.name LIKE '{$cat[$i]}'";
                    }
                    else{
                        $sql .= "AND category.name LIKE '{$cat[$i]}'";
                    }
                }
                else{
                    $sql .= "WHERE category.name LIKE '{$cat[$i]}'";
                }
            }
        }

        if($name!=null){
            if($cat!=null)
            {
                $sql .= "AND product.name LIKE :name";
            }
            else{
                $sql .= "WHERE product.name LIKE :name";
            }
        }

        if($crois!=null){
            $sql .= "ORDER BY product.price";
        }

        if($decrois!=null){
            $sql .= "ORDER BY product.price DESC";
        }


        // Exécution de la requête
        $req = $db->prepare($sql);
        if($name!=null){
            $req->execute(['name'=> '%'.htmlspecialchars($name).'%']);
        }
        else{
            $req->execute();
        }


        // Retourner les résultats (type array)

        return $req->fetchAll();
    }

  static function infoProduct(int $id){
      // Connexion à la base de données
      $db = \model\Model::connect();

      // Requête SQL
      $sql = "SELECT product.id, product.name, product.price, product.image, product.image_alt1, product.image_alt2,
        product.image_alt3, product.spec, category.name as category FROM product INNER JOIN category ON product.category = category.id WHERE product.id = :id";

      // Exécution de la requête
      $req = $db->prepare($sql);
      $req->execute(['id'=>$id]);

      // Retourner les résultats (type array)
      return $req->fetch();
  }



}