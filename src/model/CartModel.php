<?php


namespace model;


class CartModel
{
    static function listCart(){
        if(!isset($_SESSION['cart'])){
            $tab = null;
            return $tab;
        }
        // Connexion à la base de données
        $db = \model\Model::connect();

        // Requête SQL
        $sql = "SELECT product.id as id, product.name as name, product.price as price, product.image as image , category.name AS category  FROM product INNER JOIN category ON product.category = category.id ";

        $i=0;

        foreach ($_SESSION['cart']['id'] as $element){
            if($i==0){
                $sql .= "WHERE product.id = {$element} ";
            }
            else{
                $sql .= "OR product.id = {$element} ";
            }
            $i++;
        }

        // Exécution de la requête
        $req = $db->prepare($sql);
        $req->execute();

        // Retourner les résultats (type array)
        return $req->fetchAll();
    }
}