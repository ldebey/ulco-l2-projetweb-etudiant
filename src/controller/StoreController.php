<?php

namespace controller;

class StoreController {

  public function store(): void
  {
    // Communications avec la base de données
    $categories = \model\StoreModel::listCategories();
    $product = \model\StoreModel::listProducts();

    // Variables à transmettre à la vue
    $params = array(
      "title" => "Store",
      "module" => "store.php",
      "categories" => $categories,
      "product" => $product
    );

    // Faire le rendu de la vue "src/view/Template.php"
    \view\Template::render($params);
  }

  public static function product(int $id){
      $product = \model\StoreModel::infoProduct($id);
      if($product==null){
          header("Location: /store");
          exit();
      }
      $params = array(
          "title" => "Product",
          "module" => "product.php",
          "product" => $product
      );

      \view\Template::render($params);
  }

  public static function search(){
      $name = $_POST['search'];
      $cat = null;
      $crois =null;
      $decrois = null;
      if(isset($_POST['category'])){
          $cat = $_POST['category'];
      }
      if(isset($_POST['order-crois'])){
          $crois = $_POST['order-crois'];
      }
      if(isset($_POST['order-decrois'])){
          $decrois = $_POST['order-decrois'];
      }

      // Communications avec la base de données
      $categories = \model\StoreModel::listCategories();
      $product = \model\StoreModel::listSearch($name,$cat,$crois,$decrois);

      // Variables à transmettre à la vue
      $params = array(
          "title" => "Store",
          "module" => "store.php",
          "categories" => $categories,
          "product" => $product
      );

      // Faire le rendu de la vue "src/view/Template.php"
      \view\Template::render($params);
  }
}