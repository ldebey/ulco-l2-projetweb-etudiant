<?php


namespace controller;


class CartController
{
    public function cart(){

        $cart = \model\CartModel::listCart();

        // Variables à transmettre à la vue
        $params = [
            "title"  => "Cart",
            "module" => "cart.php",
            "cart" => $cart
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public static function add($id){
        if(!isset($_SESSION['cart'])){
            $_SESSION['cart'] = array();
            $_SESSION['cart']['id'] = array();
            $_SESSION['cart']['cpt'] = array();
        }
        if(array_search($id,$_SESSION['cart']['id']) !==false){
            $_SESSION['cart']['cpt'][array_search($id,$_SESSION['cart']['id'])] += $_POST['cpt'];
        }
        else{
            array_push($_SESSION['cart']['id'],$id);
            array_push( $_SESSION['cart']['cpt'],$_POST['cpt']);
        }

        header('Location: /cart');
        exit();
    }

    public static function update(){
        if(isset($_POST["cpt"])){
            if(sizeof($_SESSION["cart"]['id'])!=sizeof($_POST["id_cart"])){
                foreach ($_SESSION["cart"]['id'] as $find){
                    $test = false;
                    foreach ($_POST["id_cart"] as $search){
                        if($search == $find){
                            $test = true;
                        }
                    }
                    if($test == false){
                        unset($_SESSION['cart']['cpt'][array_search($find,$_SESSION["cart"]['id'])]);
                        unset($_SESSION['cart']['id'][array_search($find,$_SESSION["cart"]['id'])]);
                    }
                }
            }
            for($i=0;$i<sizeof($_SESSION["cart"]['id']);$i++){
                if(isset($_POST["cpt"][$i])){
                    $_SESSION['cart']['cpt'][array_search($_POST['id_cart'][$i],$_SESSION['cart']['id'])] = $_POST["cpt"][$i];
                }


            }
        }
        else{
            unset($_SESSION['cart']);
        }

        header('Location: /cart');
        exit();

    }
}