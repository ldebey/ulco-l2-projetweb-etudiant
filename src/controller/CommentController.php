<?php


namespace controller;


class CommentController
{
    public static function postComment($id){
        \model\CommentModel::insertComment($id,$_SESSION['id'],htmlspecialchars($_POST['comment']));
        header("Location: /store/{$id}");
        exit();
    }
}