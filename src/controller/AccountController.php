<?php


namespace controller;

class AccountController{
    public function account():void{
        // Variables à transmettre à la vue
        $params = [
            "title"  => "Account",
            "module" => "account.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function signin():void{
        $firstname = htmlspecialchars($_POST['userfirstname']);
        $lastname = htmlspecialchars($_POST['userlastname']);
        $mail = htmlspecialchars($_POST['usermail']);
        $password = password_hash(htmlspecialchars($_POST['userpass']),PASSWORD_DEFAULT);
        if(\model\AccountModel::signin($firstname,$lastname,$mail,$password)){
            header("Location: /account?status=signin_success");
        }
        exit();

    }

    public function login(){
        $mail = htmlspecialchars($_POST['usermail']);
        $password = htmlspecialchars($_POST['userpass']);
        if(\model\AccountModel::login($mail,$password)){
            header("Location: /store");
        }
        else{
            header("Location: /account?status=login_fail");
        }
        exit();
    }

    public function logout(){
        session_destroy();
        header("Location: /account?status=logout");
        exit();
    }

    public function infos(){
        // Variables à transmettre à la vue
        $params = [
            "title"  => "Infos",
            "module" => "infos.php"
        ];

        // Faire le rendu de la vue "src/view/Template.php"
        \view\Template::render($params);
    }

    public function update(){
        $firstname = htmlspecialchars($_POST['firstname']);
        $lastname = htmlspecialchars($_POST['lastname']);
        $mail = htmlspecialchars($_POST['mail']);
        \model\AccountModel::update($firstname,$lastname,$mail);
        header("Location: /account/info?status=update_success");
        exit();
    }
}