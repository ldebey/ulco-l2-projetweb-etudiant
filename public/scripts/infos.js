document.addEventListener('DOMContentLoaded',function(){
    console.log("toto")
    let fname = document.getElementById("firstname");
    let lname = document.getElementById("lastname");
    let mail = document.getElementById("mail");
    let emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

    mail.addEventListener('input',function(){
        if(emailReg.test(mail.value)){

            mail.setAttribute('class','valid')

        }
        else{
            mail.setAttribute('class','invalid')

        }
    })

    lname.addEventListener('input',function (){
        if(lname.value.length<=2){
            lname.setAttribute('class','invalid')

        }
        else
        {
            lname.setAttribute('class','valid')

        }
    })

    fname.addEventListener('input',function (){
        if(fname.value.length<=2){
            fname.setAttribute('class','invalid')

        }
        else
        {
            fname.setAttribute('class','valid')

        }
    })
})