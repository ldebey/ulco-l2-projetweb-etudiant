document.addEventListener('DOMContentLoaded',function(){
    let cpt = 1;
    let display = document.querySelector(".product-images").children[0];
    let images = document.querySelector(".product-miniatures").children;
    let bouton_plus = document.getElementById("+");
    let bouton_moins = document.getElementById("-");
    let affiche_cpt = document.getElementById("affiche_cpt");
    let input_cpt = document.getElementById("input_cpt");
    input_cpt.value = cpt;


    for(let image of images){
        image.addEventListener('click',function(){
            display.setAttribute("src",image.children[0].getAttribute("src")) ;
        })
    }

    bouton_plus.addEventListener('click',function (){
        if (document.getElementsByClassName("box error")[0]!=null)
        {
            document.getElementsByClassName("box error")[0].remove();
        }
        if(cpt<5){
            cpt++;
            affiche_cpt.innerHTML = cpt;
            input_cpt.value = cpt;
        }
        else{
            let div = document.createElement("div");
            div.setAttribute("class","box error");
            div.innerHTML = "Quantité maximale autorisée !";
            document.querySelector(".product-infos").appendChild(div);
        }
    })

    bouton_moins.addEventListener('click',function (){
        if (document.getElementsByClassName("box error")[0]!=null)
        {
            document.getElementsByClassName("box error")[0].remove();
        }
        if(cpt>1){
            cpt--;
            affiche_cpt.innerHTML = cpt;
            input_cpt.value = cpt;
        }
        else{
            let div = document.createElement("div");
            div.setAttribute("class","box error");
            div.innerHTML = "Quantité maximale autorisée !";
            document.querySelector(".product-infos").appendChild(div);
        }
    })




})