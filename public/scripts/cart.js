let box = document.getElementById("total");
let prices = document.getElementsByClassName("price");
let cpts = document.getElementsByClassName("cpt");
let group_button = new Array() ;

function calcTotalPrice(){
    let i=0;
    let sum = 0;
    for(let price of prices){
        sum += parseInt(price.innerHTML)*cpts[i].value;
        i++;
    }
    box.innerHTML = sum + "€";
}
document.addEventListener('DOMContentLoaded',function(){
    calcTotalPrice();
    for(let j=0;j<cpts.length;j++){
        group_button[j] = document.getElementsByClassName("group["+j+"]");

    }
    // 0 pour le - 1 pour affiche_cpt 2 pour le input cpt et 3 pour le +
    for(let group of group_button){
        group[0].addEventListener('click',function(){
            if (document.getElementsByClassName("box info")[0]!=null)
            {
                document.getElementsByClassName("box info")[0].remove();
            }
            if (document.getElementsByClassName("box error")[0]!=null)
            {
                document.getElementsByClassName("box error")[0].remove();
            }
            if(group[2].value <= 1){
                let div = document.createElement("div");
                div.setAttribute("class","box info");
                div.innerHTML = "Article supprimé du panier";
                document.querySelector("#cart").appendChild(div);
                group[0].parentElement.parentElement.parentElement.remove();
                prices = document.getElementsByClassName("price");
                cpts = document.getElementsByClassName("cpt");
            }
            else{
                group[2].value = parseInt(group[2].value) - 1 ;
                group[1].innerHTML = group[2].value;
            }

            calcTotalPrice();
        });

        group[3].addEventListener('click',function(){
            if (document.getElementsByClassName("box info")[0]!=null)
            {
                document.getElementsByClassName("box info")[0].remove();
            }
            if (document.getElementsByClassName("box error")[0]!=null)
            {
                document.getElementsByClassName("box error")[0].remove();
            }
            if(group[2].value >= 5){
                let div = document.createElement("div");
                div.setAttribute("class","box error");
                div.innerHTML = "Quantité maximale autorisée !";
                document.querySelector("#cart").appendChild(div);
            }
            else{
                group[2].value = parseInt(group[2].value) + 1 ;
                group[1].innerHTML = group[2].value;
            }
            calcTotalPrice();
        });
    }

})