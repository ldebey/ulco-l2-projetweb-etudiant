document.addEventListener('DOMContentLoaded',function (){
    let lastname = document.getElementsByName("userlastname")[0];
    let firstname = document.getElementsByName("userfirstname")[0];
    let emailReg = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let passRef = /^(?=.*?[a-z])(?=.*?[0-9]).{6,}$/
    let mails = document.getElementsByName("usermail");
    let passwords = document.getElementsByName("userpass");

    lastname.addEventListener('input',function (){
        if(lastname.value.length<=2){
            lastname.setAttribute('class','invalid')
            lastname.previousElementSibling.setAttribute('class','invalid')
        }
        else
        {
            lastname.setAttribute('class','valid')
            lastname.previousElementSibling.setAttribute('class','valid')
        }
    })

    firstname.addEventListener('input',function (){
        if(firstname.value.length<=2){
            firstname.setAttribute('class','invalid')
            firstname.previousElementSibling.setAttribute('class','invalid')
        }
        else
        {
            firstname.setAttribute('class','valid')
            firstname.previousElementSibling.setAttribute('class','valid')
        }
    })

    for(let mail of mails){
        mail.addEventListener('input',function(){
            if(emailReg.test(mail.value)){

                mail.setAttribute('class','valid')
                mail.previousElementSibling.setAttribute('class','valid')
            }
            else{
                mail.setAttribute('class','invalid')
                mail.previousElementSibling.setAttribute('class','invalid')
            }
        })
    }

    for(let password of passwords){
        password.addEventListener('input',function(){
            if(passRef.test(password.value)){
                password.setAttribute('class','valid')
                password.previousElementSibling.setAttribute('class','valid')
                if(password === passwords[1] || password === passwords[2]){
                    if(passwords[2].value!==passwords[1].value){
                        passwords[2].setAttribute('class','invalid')
                        passwords[2].previousElementSibling.setAttribute('class','invalid')
                    }
                    else{
                        passwords[2].setAttribute('class','valid')
                        passwords[2].previousElementSibling.setAttribute('class','valid')
                    }
                }


            }
            else
            {
                password.setAttribute('class','invalid')
                password.previousElementSibling.setAttribute('class','invalid')
            }


        })
    }


})